<?php

/**
 * @file
 */

/**
 * Implements hook_ckeditor_plugin().
 */
function media_ckeditor_integration_ckeditor_plugin() {
  $plugins = array();

  // Override the default CKEditor Media plugin.
  $plugins['media'] = array(
    'name' => 'media',
    'desc' => t('Plugin for embedding files using Media CKEditor'),
    'path' => '%base_path%' . drupal_get_path('module', 'media_ckeditor_integration') . '/js/plugins/media/',
    'buttons' => array(
      'Media' => array(
        'icon' => 'images/icon.gif',
        'label' => 'Add media',
      ),
    ),
    'default' => 'f',
  );

  return $plugins;
}
